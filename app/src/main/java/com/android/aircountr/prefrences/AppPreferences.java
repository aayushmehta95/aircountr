package com.android.aircountr.prefrences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by gaurav on 5/1/2016.
 */
public class AppPreferences {
    private static SharedPreferences pref;
    private static Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "AircountrPrefs";
    public static String KEY_ACCESS_TOKEN = "token";
    public static String KEY_MERCHANT_ID = "merchantId";

    public AppPreferences(Context context) {
        this._context = context;
        this.pref = this._context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.commit();
    }

    public static String getAccessToken(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, "");
    }

    public static void setAccessToken(Context _context, String accessToken) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_ACCESS_TOKEN, accessToken);
        editor.commit();
    }

    public static String getMerchantId(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_MERCHANT_ID, "");
    }

    public static void setMerchantId(Context _context, String merchantId) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_MERCHANT_ID, merchantId);
        editor.commit();
    }
}