package com.android.aircountr.smoochIo;

import android.os.Bundle;

import com.android.aircountr.BaseActivity;
import com.android.aircountr.R;

import io.smooch.ui.ConversationActivity;

/**
 * Created by Saahil on 11/06/16.
 */
public class SmoochIO extends BaseActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConversationActivity.show(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
