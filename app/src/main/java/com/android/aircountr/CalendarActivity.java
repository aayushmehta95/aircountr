package com.android.aircountr;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.aircountr.adapters.CalendarGridAdapter;
import com.android.aircountr.adapters.CategorySpinnerAdapter;
import com.android.aircountr.adapters.VendorSpinnerAdapter;
import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.objects.CalendarListItem;
import com.android.aircountr.objects.CategoryListItem;
import com.android.aircountr.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by gaurav on 4/27/2016.
 */
public class CalendarActivity extends BaseActivity implements View.OnClickListener, UrlConstants {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private ImageView iv_export;
    private TextView tv_categoryText;
    private TextView tv_category;
    private TextView tv_vendorText;
    private TextView tv_vendor;
    private ImageView iv_previousDateBtn;
    private TextView tv_selectedDate;
    private ImageView iv_nextDateBtn;
    private LinearLayout ll_daysOfWeeks;
    private GridView gv_calendar;
    private TextView tv_errNoDataMsg;
    private int BTN_CATEGORY = 1, BTN_VENDOR = 2;
    private ArrayList<CategoryListItem.Vendors> vendorsDataList;
    private Calendar cal;
    private CalendarGridAdapter mCalendarGridAdapter;
    private ArrayList<CalendarListItem> mCalendarDataList;
    private String strTimeStamp = null, categoryId = "", vendorId = "";
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        vendorsDataList = new ArrayList<>();
        init();

        cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));

        mCalendarDataList = new ArrayList<>();
        mCalendarGridAdapter = new CalendarGridAdapter(CalendarActivity.this, mCalendarDataList);
        gv_calendar.setAdapter(mCalendarGridAdapter);

        strTimeStamp = String.valueOf(cal.getTimeInMillis());
        sendGetCalendarRequest(String.valueOf(cal.getTimeInMillis()));

        gv_calendar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "T : " + strTimeStamp + " D : " + String.valueOf(mCalendarDataList.get(position).getDate()));
                Intent intent = new Intent(CalendarActivity.this, InvoiceListActivity.class);
                intent.putExtra(TIMESTAMP, strTimeStamp);
                intent.putExtra(DAY_OF_MONTH, String.valueOf(mCalendarDataList.get(position).getDate()));
                startActivity(intent);
            }
        });
    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        iv_export = (ImageView) findViewById(R.id.iv_export);
        tv_categoryText = (TextView) findViewById(R.id.tv_categoryText);
        tv_category = (TextView) findViewById(R.id.tv_category);
        tv_vendorText = (TextView) findViewById(R.id.tv_vendorText);
        tv_vendor = (TextView) findViewById(R.id.tv_vendor);
        iv_previousDateBtn = (ImageView) findViewById(R.id.iv_previousDateBtn);
        tv_selectedDate = (TextView) findViewById(R.id.tv_selectedDate);
        iv_nextDateBtn = (ImageView) findViewById(R.id.iv_nextDateBtn);
        ll_daysOfWeeks = (LinearLayout) findViewById(R.id.ll_daysOfWeeks);
        TextView tv_sunText = (TextView) findViewById(R.id.tv_sunText);
        TextView tv_monText = (TextView) findViewById(R.id.tv_monText);
        TextView tv_tueText = (TextView) findViewById(R.id.tv_tueText);
        TextView tv_wedText = (TextView) findViewById(R.id.tv_wedText);
        TextView tv_thuText = (TextView) findViewById(R.id.tv_thuText);
        TextView tv_friText = (TextView) findViewById(R.id.tv_friText);
        TextView tv_satText = (TextView) findViewById(R.id.tv_satText);
        gv_calendar = (GridView) findViewById(R.id.gv_calendar);
        tv_errNoDataMsg = (TextView) findViewById(R.id.tv_errNoDataMsg);

        tv_pageTitle.setTypeface(SEMIBOLD);
        tv_categoryText.setTypeface(REGULAR);
        tv_category.setTypeface(REGULAR);
        tv_vendorText.setTypeface(REGULAR);
        tv_vendor.setTypeface(REGULAR);
        tv_selectedDate.setTypeface(SEMIBOLD);
        tv_sunText.setTypeface(REGULAR);
        tv_monText.setTypeface(REGULAR);
        tv_tueText.setTypeface(REGULAR);
        tv_wedText.setTypeface(REGULAR);
        tv_thuText.setTypeface(REGULAR);
        tv_friText.setTypeface(REGULAR);
        tv_satText.setTypeface(REGULAR);
        tv_errNoDataMsg.setTypeface(SEMIBOLD);

        tv_category.setOnClickListener(this);
        tv_vendor.setOnClickListener(this);
        iv_backBtn.setOnClickListener(this);
        iv_previousDateBtn.setOnClickListener(this);
        iv_nextDateBtn.setOnClickListener(this);
        iv_export.setOnClickListener(this);
    }

    private void selectOptionPopUp(final int btnType) {
        final Dialog dialog = new Dialog(CalendarActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_category_dropdown);
        ListView lv_category = (ListView) dialog.findViewById(R.id.lv_category);
        if (btnType == BTN_CATEGORY) {
            CategorySpinnerAdapter mCategorySpinnerAdapter = new CategorySpinnerAdapter(CalendarActivity.this, ((AircountrApplication) getApplicationContext()).mCategoryListItems);
            lv_category.setAdapter(mCategorySpinnerAdapter);

            lv_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tv_category.setText(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getCategoryName());
                    tv_vendor.setText(getResources().getString(R.string.txt_all_vendors));
                    categoryId = ((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getCategoryId();
                    vendorId = "";
                    vendorsDataList = new ArrayList<CategoryListItem.Vendors>(((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getVendorsDataList());
                    Log.d(TAG, "Vendor list size : " + vendorsDataList.size() + "position : " + position + "Main List Size : " + ((AircountrApplication) getApplicationContext()).mCategoryListItems.get(position).getVendorsDataList().size());
                    dialog.dismiss();
                    sendGetCalendarRequest(strTimeStamp);
                }
            });

            dialog.show();
        } else if (btnType == BTN_VENDOR) {
            VendorSpinnerAdapter mVendorSpinnerAdapter = new VendorSpinnerAdapter(CalendarActivity.this, vendorsDataList);
            lv_category.setAdapter(mVendorSpinnerAdapter);
            lv_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tv_vendor.setText(vendorsDataList.get(position).getVendorName());
                    vendorId = vendorsDataList.get(position).getVendorId();
                    dialog.dismiss();
                    sendGetCalendarRequest(strTimeStamp);
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_backBtn:
                CalendarActivity.this.finish();
                break;
            case R.id.tv_category:
                if (((AircountrApplication) getApplicationContext()).mCategoryListItems != null && ((AircountrApplication) getApplicationContext()).mCategoryListItems.size() > 0)
                    selectOptionPopUp(BTN_CATEGORY);
                else
                    displayToast("NO CATEGORY FOUND");
                break;
            case R.id.tv_vendor:
                if (vendorsDataList != null && vendorsDataList.size() > 0)
                    selectOptionPopUp(BTN_VENDOR);
                else
                    displayToast("NO VENDOR FOUND");
                break;
            case R.id.iv_previousDateBtn:
                cal.add(Calendar.MONTH, -1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                sendGetCalendarRequest(String.valueOf(cal.getTimeInMillis()));
                break;
            case R.id.iv_nextDateBtn:
                cal.add(Calendar.MONTH, 1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                sendGetCalendarRequest(String.valueOf(cal.getTimeInMillis()));
                break;
            case R.id.iv_export:
                if (strTimeStamp != null)
                    sendReportExportRequest();
                break;
        }
    }

    private void sendGetCalendarRequest(String timeStamp) {
        if (!AppPreferences.getMerchantId(CalendarActivity.this).equals("") && !AppPreferences.getAccessToken(CalendarActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new GetCalendarAsync().execute(timeStamp);
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class GetCalendarAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "Response : " + s);
            try {
                if (s != null) {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray invoiceArray = response.getJSONArray("inv");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            mCalendarDataList.clear();
                            for (int i = 0; i < invoiceArray.length(); i++) {
                                JSONObject invoiceObj = invoiceArray.getJSONObject(i);
                                CalendarListItem rowData = new CalendarListItem();
                                JSONObject dayObj = invoiceObj.getJSONObject("_id");
                                int day = dayObj.getInt("day");
                                String amount = invoiceObj.getString("totalinvoice");
                                int numberOfInvoice = invoiceObj.getInt("numberofinvoices");
                                JSONArray commentsArray = invoiceObj.getJSONArray("comments");
                                ArrayList<String> commentsList = new ArrayList<>();
                                if (commentsArray != null && commentsArray.length() > 0) {
                                    for (int j = 0; j < commentsArray.length(); j++) {
                                        String comment = commentsArray.getString(j);
                                        commentsList.add(comment);
                                    }
                                }
                                rowData.setDate(day);
                                rowData.setAmount(amount);
                                rowData.setNumberOfInvoice(numberOfInvoice);
                                rowData.setComments(commentsList);
                                mCalendarDataList.add(rowData);
                            }
                            tv_errNoDataMsg.setVisibility(View.GONE);
                            ll_daysOfWeeks.setVisibility(View.VISIBLE);
                            gv_calendar.setVisibility(View.VISIBLE);
                        }
                    } else {
                        tv_errNoDataMsg.setVisibility(View.VISIBLE);
                        tv_errNoDataMsg.setText(response.getString("msg"));
                        ll_daysOfWeeks.setVisibility(View.GONE);
                        gv_calendar.setVisibility(View.GONE);
                    }
                } else {
                    showDialog("Alert", "oops! something is wrong", "OK");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mCalendarGridAdapter.onDataSetChanged(mCalendarDataList);
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.getCalendarDataUrl(AppPreferences.getMerchantId(CalendarActivity.this), params[0], vendorId, categoryId);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(CalendarActivity.this));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    private void sendReportExportRequest() {
        if (!AppPreferences.getMerchantId(CalendarActivity.this).equals("") && !AppPreferences.getAccessToken(CalendarActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new ReportExportAsync().execute(strTimeStamp);
            } else
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        } else {
            showDialog("Alert", "You seems to be logged out", "OK");
        }
    }

    private class ReportExportAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Exporting...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject response = new JSONObject(s);
                boolean success = response.getBoolean("success");
                String msg = response.getString("msg");
                if (success)
                    displayToast(msg);
                else
                    showDialog("Alert", msg, "OK");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.exportReportsUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(CalendarActivity.this));
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("merchantId", AppPreferences.getMerchantId(CalendarActivity.this));
                jsonObject.put("timestamp", params[0]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
}
