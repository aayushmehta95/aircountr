package com.android.aircountr;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.aircountr.adapters.DrawerListAdapter;
import com.android.aircountr.fragment.ExpenseFragment;
import com.android.aircountr.fragment.HomeFragment;
import com.android.aircountr.fragment.PredictorFragment;
import com.android.aircountr.smoochIo.SmoochIO;

import java.util.ArrayList;
import java.util.List;

import io.karim.MaterialTabs;


/**
 * Created by gaurav on 4/26/2016.
 */
public class HomeActivity extends FragmentActivity {
    public FragmentManager fragment_mgr;
    private Fragment mContent;
    public Typeface REGULAR, SEMIBOLD;
    public DrawerLayout drawerLayout;
    private ImageView iv_leftMenu;
    public EditText et_search;
    private ImageView iv_searchBtn;
    private ImageView iv_calenderBtn;
    private LinearLayout ll_leftDrawerLayout;
    private TextView tv_merchantName;
    private ListView lv_drawerList;
    public ProgressDialog processing;
    private DrawerListAdapter mDrawerListAdapter;
    private MaterialTabs tabs;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");

/*        fragment_mgr = getSupportFragmentManager();
        if (savedInstanceState != null) {
            mContent = getSupportFragmentManager()
                    .findFragmentByTag("mContent");
        }

        if (mContent == null)
            mContent = new HomeFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commit();
*/
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        iv_leftMenu = (ImageView) findViewById(R.id.iv_leftMenu);
        et_search = (EditText) findViewById(R.id.et_search);
        iv_searchBtn = (ImageView) findViewById(R.id.iv_searchBtn);
        iv_calenderBtn = (ImageView) findViewById(R.id.iv_calenderBtn);
        ll_leftDrawerLayout = (LinearLayout) findViewById(R.id.tv_leftDrawerLayout);
        tv_merchantName = (TextView) findViewById(R.id.tv_merchantName);
        lv_drawerList = (ListView) findViewById(R.id.lv_drawerList);
        tabs = (MaterialTabs) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        ArrayList<String> tabsList = new ArrayList<>();
        tabsList.add("Home");
        tabsList.add("My Invoices");
        setupViewPager(viewPager, tabsList);

        ArrayList<String> drawerList = new ArrayList<>();
        drawerList.add(0, "My Invoices");
        drawerList.add(1, "My Vendors");
        drawerList.add(2, "Reports");
        drawerList.add(3, "Rating");
        drawerList.add(4, "Share");
        drawerList.add(5, "Settings");
        drawerList.add(6, "Contact Us");
        drawerList.add(7, "Wallet");
        mDrawerListAdapter = new DrawerListAdapter(HomeActivity.this, drawerList);
        lv_drawerList.setAdapter(mDrawerListAdapter);

        et_search.setTypeface(REGULAR);
        tv_merchantName.setTypeface(SEMIBOLD);

        drawerLayout.setDrawerListener(new LeftMenuDrawer());

        iv_leftMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(ll_leftDrawerLayout);
            }
        });

        lv_drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (drawerLayout.isDrawerOpen(ll_leftDrawerLayout)) {
                    drawerLayout.closeDrawer(ll_leftDrawerLayout);
                }
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        switchActivity(HomeActivity.this, MyVendorsActivity.class);
                        break;
                    case 3:
                        switchActivity(HomeActivity.this, ExpenseActivity.class);
                        break;
                    case 6:
                        switchActivity(HomeActivity.this, SmoochIO.class);
                        break;
                    case 7:
                        switchActivity(HomeActivity.this, WalletActivity.class);
                        break;
                }
            }
        });

        iv_calenderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchActivity(HomeActivity.this, CalendarActivity.class);
            }
        });
    }

    public <T> void switchActivity(Context context, Class<T> startActivity) {

        Intent intent = new Intent(context, startActivity);
        startActivity(intent);
    }

    public void displayToast(int id) {
        Toast.makeText(getApplicationContext(), getResources().getString(id),
                Toast.LENGTH_SHORT).show();
    }

    public void displayToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void switchContent(Fragment fragment, int id, boolean addToBackStack) {
/*
        mContent = fragment;
        if (addToBackStack) {
            fragment_mgr.beginTransaction()
                    .replace(R.id.content_frame, fragment, "mContent")
                    .addToBackStack(null).commit();

        } else {
            fragment_mgr.beginTransaction().replace(R.id.content_frame, fragment)
                    .commit();
        }*/
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public class LeftMenuDrawer implements DrawerLayout.DrawerListener {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {
        }

        @Override
        public void onDrawerClosed(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    }

    public void showDialog(String title, String msg, String btnText) {
        AlertDialog alertdialog = new AlertDialog.Builder(this).create();
        alertdialog.setCancelable(false);
        alertdialog.setTitle(title);
        alertdialog.setMessage(msg);
        alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, btnText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertdialog.show();
    }

    public void showLoading(String message, boolean cancelable) {
        processing = new ProgressDialog(this);
        processing.setMessage(message);
        processing.setCancelable(cancelable);
        processing.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        processing.setCanceledOnTouchOutside(cancelable);
        if (!processing.isShowing()) {
            processing.show();
        }
    }

    public void hideLoading() {
        if (processing != null)
            processing.dismiss();
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<String> mDataList) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mDataList);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager, List<String> mFragmentTitleList) {
            super(manager);
            this.mFragmentTitleList = mFragmentTitleList;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new HomeFragment();
                    break;
                case 1:
                    fragment = new HomeFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}