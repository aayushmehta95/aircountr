package com.android.aircountr;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.prefrences.AppPreferences;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by gaurav on 4/27/2016.
 */
public class BillDetailsActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, UrlConstants {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private TextView tv_dateText;
    private TextView tv_date;
    private TextView tv_errDate;
    private TextView tv_AmountText;
    private EditText et_amount;
    private TextView tv_errAmount;
    private TextView tv_categoryText;
    private TextView tv_category;
    private TextView tv_vendorText;
    private AutoCompleteTextView actv_vendor;
    private TextView tv_errVendor;
    private TextView tv_commentText;
    private EditText et_comments;
    private TextView tv_uptoText;
    private TextView tv_addImgText;
    private ImageView iv_img1;
    private TextView tv_img1Text;
    private ImageView iv_img2;
    private TextView tv_img2Text;
    private ImageView iv_img3;
    private TextView tv_img3Text;
    private TextView tv_saveBtn;
    private int imageBtnType = 0;
    private final int CAMERA_REQUEST = 101, GALLERY_REQUEST = 100;
    private byte[] image_byte;
    private ByteArrayBody bab;
    private String categoryId = "", categoryName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_details);
        init();

        Intent intent = getIntent();
        categoryId = intent.getStringExtra(CATEGORY_ID);
        categoryName = intent.getStringExtra(CATEGORY_NAME);

        tv_category.setText(categoryName);
        tv_dateText.setOnClickListener(this);
        tv_date.setOnClickListener(this);
        iv_img1.setOnClickListener(this);
        iv_img2.setOnClickListener(this);
        iv_img3.setOnClickListener(this);
        iv_backBtn.setOnClickListener(this);
        et_amount.addTextChangedListener(new GenericTextWatcher(et_amount));
        actv_vendor.addTextChangedListener(new GenericTextWatcher(actv_vendor));
        tv_saveBtn.setOnClickListener(this);

    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_dateText = (TextView) findViewById(R.id.tv_dateText);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_errDate = (TextView) findViewById(R.id.tv_errDate);
        tv_AmountText = (TextView) findViewById(R.id.tv_AmountText);
        et_amount = (EditText) findViewById(R.id.et_amount);
        tv_errAmount = (TextView) findViewById(R.id.tv_errAmount);
        tv_categoryText = (TextView) findViewById(R.id.tv_categoryText);
        tv_category = (TextView) findViewById(R.id.tv_category);
        tv_vendorText = (TextView) findViewById(R.id.tv_vendorText);
        actv_vendor = (AutoCompleteTextView) findViewById(R.id.actv_vendor);
        tv_errVendor = (TextView) findViewById(R.id.tv_errVendor);
        tv_commentText = (TextView) findViewById(R.id.tv_commentText);
        et_comments = (EditText) findViewById(R.id.et_comments);
        tv_uptoText = (TextView) findViewById(R.id.tv_uptoText);
        tv_addImgText = (TextView) findViewById(R.id.tv_addImgText);
        iv_img1 = (ImageView) findViewById(R.id.iv_img1);
        tv_img1Text = (TextView) findViewById(R.id.tv_img1Text);
        iv_img2 = (ImageView) findViewById(R.id.iv_img2);
        tv_img2Text = (TextView) findViewById(R.id.tv_img2Text);
        iv_img3 = (ImageView) findViewById(R.id.iv_img3);
        tv_img3Text = (TextView) findViewById(R.id.tv_img3Text);
        tv_saveBtn = (TextView) findViewById(R.id.tv_saveBtn);

        tv_pageTitle.setTypeface(SEMIBOLD);
        tv_dateText.setTypeface(REGULAR);
        tv_date.setTypeface(REGULAR);
        tv_errDate.setTypeface(REGULAR);
        tv_AmountText.setTypeface(REGULAR);
        et_amount.setTypeface(REGULAR);
        tv_errAmount.setTypeface(REGULAR);
        tv_categoryText.setTypeface(REGULAR);
        tv_category.setTypeface(REGULAR);
        tv_vendorText.setTypeface(REGULAR);
        actv_vendor.setTypeface(REGULAR);
        tv_errVendor.setTypeface(REGULAR);
        tv_commentText.setTypeface(REGULAR);
        et_comments.setTypeface(REGULAR);
        tv_uptoText.setTypeface(REGULAR);
        tv_addImgText.setTypeface(REGULAR);
        tv_img1Text.setTypeface(REGULAR);
        tv_img2Text.setTypeface(REGULAR);
        tv_img3Text.setTypeface(REGULAR);
        tv_saveBtn.setTypeface(REGULAR);
    }

    public void datePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                BillDetailsActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.vibrate(false);
        dpd.dismissOnPause(true);
        dpd.showYearPickerFirst(false);
        dpd.setAccentColor(getResources().getColor(R.color.theme_color_blue));
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        tv_date.setText(date);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.tv_date:
                datePickerDialog();
                break;
            case R.id.tv_dateText:
                datePickerDialog();
                break;
            case R.id.iv_img1:
                imageBtnType = 1;
                selectImgOptionPopUp();
                break;
            case R.id.iv_img2:
//                imageBtnType = 2;
//                selectImgOptionPopUp();
                break;
            case R.id.iv_img3:
//                imageBtnType = 3;
//                selectImgOptionPopUp();
                break;
            case R.id.tv_saveBtn:
                if (checkValidation()) {
                    sendBillDetailRequest(categoryId, et_amount.getText().toString().trim(), et_comments.getText().toString().trim(), tv_date.getText().toString().trim(), actv_vendor.getText().toString().trim());
                }
                break;
            case R.id.iv_backBtn:
                BillDetailsActivity.this.finish();
                break;
        }
    }

    private boolean checkValidation() {
        if (tv_date.getText().toString().equals("")) {
            tv_errDate.setVisibility(View.VISIBLE);
            tv_errDate.setText("Plz select date");
            return false;
        } else if (et_amount.getText().toString().trim().isEmpty()) {
            tv_errAmount.setVisibility(View.VISIBLE);
            tv_errAmount.setText("Plz enter amount");
            return false;
        } else if (actv_vendor.getText().toString().trim().isEmpty()) {
            tv_errVendor.setVisibility(View.VISIBLE);
            tv_errVendor.setText("Plz enter vendor name");
            return false;
        } else {
            tv_errDate.setVisibility(View.GONE);
            tv_errAmount.setVisibility(View.GONE);
            tv_errVendor.setVisibility(View.GONE);
            return true;
        }
    }

    private void selectImgOptionPopUp() {
        final Dialog dialog = new Dialog(BillDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_select_img_layout);
        TextView tv_cameraBtn = (TextView) dialog.findViewById(R.id.tv_cameraBtn);
        TextView tv_galleryBtn = (TextView) dialog.findViewById(R.id.tv_galleryBtn);
        TextView tv_cancelBtn = (TextView) dialog.findViewById(R.id.tv_cancelBtn);

        tv_cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
                dialog.dismiss();
            }
        });

        tv_galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                dialog.dismiss();
            }
        });

        tv_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void sendBillDetailRequest(String categoryId, String amount, String comment, String date, String vendorName) {
        if (!AppPreferences.getMerchantId(BillDetailsActivity.this).equals("") && !AppPreferences.getAccessToken(BillDetailsActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new BillDetailAsync().execute(categoryId, amount, comment, date, vendorName);
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            switchActivity(BillDetailsActivity.this, SignInActivity.class);
        }
    }

    private class BillDetailAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        displayToast(msg);
                        BillDetailsActivity.this.finish();
                    } else {
                        showDialog("Aler", msg, "OK");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.uploadBillDetail();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(BillDetailsActivity.this));
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            try {
                entity.addPart("vendorId", new StringBody(""));
                entity.addPart("merchantId", new StringBody(AppPreferences.getMerchantId(BillDetailsActivity.this)));
                entity.addPart("categoryId", new StringBody(params[0]));
                entity.addPart("invoiceamount", new StringBody(params[1]));
                entity.addPart("isautomode", new StringBody("false"));
                entity.addPart("comments", new StringBody(params[2]));
                entity.addPart("createdDate", new StringBody(params[3]));
                entity.addPart("vendorname", new StringBody(params[4]));
                entity.addPart("categoryname", new StringBody(categoryName));
                entity.addPart("isprocessed", new StringBody("true"));
                if (bab != null)
                    entity.addPart("invoice", bab);

                httpPost.setEntity(entity);
                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "Response : " + _response);
            return _response;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        Uri selectedImage = data.getData();
                        iv_img1.setImageURI(selectedImage);
                        try {
                            FileInputStream imageStream = (FileInputStream) getContentResolver().openInputStream(selectedImage);
                            Bitmap selectedImg = BitmapFactory.decodeStream(imageStream);
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            selectedImg.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
                            image_byte = outputStream.toByteArray();
                            bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".jpg");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        displayToast("Selected image not found");
                    }
                }
                break;
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    iv_img1.setImageBitmap(photo);
                    Uri tempUri = getImageUri(getApplicationContext(), photo);
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    Log.d(TAG, "A PATH : " + finalFile.getAbsolutePath());
                    Log.d(TAG, "B PATH : " + finalFile.getPath());
                    Bitmap selectedImg = BitmapFactory.decodeFile(finalFile.getAbsolutePath());
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    selectedImg.compress(Bitmap.CompressFormat.PNG, 70, outputStream);
                    image_byte = outputStream.toByteArray();
                    bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".png");
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);

        if (tv_date.getText().toString().equals("")) {
            tv_errDate.setVisibility(View.VISIBLE);
            tv_errDate.setText("Plz select date");
        } else {
            tv_errDate.setVisibility(View.GONE);
        }
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.et_amount:
                    if (text.length() > 0)
                        tv_errAmount.setVisibility(View.GONE);
                    else {
                        tv_errAmount.setVisibility(View.VISIBLE);
                        tv_errAmount.setText("Plz enter amount");
                    }
                    if (tv_date.getText().toString().trim().isEmpty()) {
                        tv_errDate.setVisibility(View.VISIBLE);
                        tv_errDate.setText("Plz select date");
                    } else {
                        tv_errDate.setVisibility(View.GONE);
                    }
                    break;
                case R.id.actv_vendor:
                    if (text.length() > 0)
                        tv_errVendor.setVisibility(View.GONE);
                    else {
                        tv_errVendor.setVisibility(View.VISIBLE);
                        tv_errVendor.setText("Plz enter vendor name");
                    }
                    if (tv_date.getText().toString().trim().isEmpty()) {
                        tv_errDate.setVisibility(View.VISIBLE);
                        tv_errDate.setText("Plz select date");
                    } else {
                        tv_errDate.setVisibility(View.GONE);
                    }
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}