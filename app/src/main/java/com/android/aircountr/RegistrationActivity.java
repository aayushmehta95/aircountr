package com.android.aircountr;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.objects.GPSAddressListItems;
import com.android.aircountr.prefrences.AppPreferences;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;


/**
 * Created by gaurav on 4/25/2016.
 */
public class RegistrationActivity extends BaseActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener, CompoundButton.OnCheckedChangeListener, UrlConstants {

    private TextView tv_pageTitle;
    private ImageView iv_backBtn;
    private EditText et_yourName;
    private EditText et_businessName;
    private TextView tv_merchantTypeText;
    private RadioGroup rg_radioGroup;
    private RadioButton rb_restaurant;
    private RadioButton rb_retailer;
    private RadioButton rb_other;
    private EditText et_typeToAdd;
    private EditText et_emailAddress;
    private EditText et_setPassword;
    private EditText et_confirmPassword;
    private EditText et_phoneNumber;
    private AutoCompleteTextView et_location;
    private TextView tv_operationalHrsText;
    private TextView tv_fromText;
    private TextView tv_fromTime;
    private TextView tv_toText;
    private TextView tv_toTime;
    private EditText et_employees;
    private CheckBox cb_privacyTerms;
    private TextView tv_privacyTerms;
    private TextView tv_registerBtn;
    private TextView tv_errYourName, tv_errBusinessName, tv_errTypeToAdd, tv_errEmailAddress, tv_errSetPassword, tv_errConfirmPassword, tv_errPhoneNumber, tv_errLocation;
    private int BTN_TYPE = 0;
    private String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private String TAG = this.getClass().getSimpleName();
    private String typeOfMerchant = "";
    private GPSAddressListItems rowData;
    private ArrayList<GPSAddressListItems> addressDataList;
    private String _placeId = "";
    private double lat = 0, lng = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        init();

        iv_backBtn.setOnClickListener(this);
        tv_fromTime.setOnClickListener(this);
        tv_toTime.setOnClickListener(this);
        tv_registerBtn.setOnClickListener(this);
        et_yourName.addTextChangedListener(new GenericTextWatcher(et_yourName));
        et_businessName.addTextChangedListener(new GenericTextWatcher(et_businessName));
        et_typeToAdd.addTextChangedListener(new GenericTextWatcher(et_typeToAdd));
        et_emailAddress.addTextChangedListener(new GenericTextWatcher(et_emailAddress));
        et_setPassword.addTextChangedListener(new GenericTextWatcher(et_setPassword));
        et_confirmPassword.addTextChangedListener(new GenericTextWatcher(et_confirmPassword));
        et_phoneNumber.addTextChangedListener(new GenericTextWatcher(et_phoneNumber));
        et_location.addTextChangedListener(new GenericTextWatcher(et_location));
        rb_restaurant.setOnCheckedChangeListener(this);
        rb_retailer.setOnCheckedChangeListener(this);
        rb_other.setOnCheckedChangeListener(this);
        et_location.setThreshold(1);
        et_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                _placeId = addressDataList.get(position).getPlaceId();
                sendPlaceDetailRequest(_placeId);
            }
        });

        SpannableString strPrivacy = new SpannableString("By registering you agree to the User Registration Terms & Privacy Policy");
        strPrivacy.setSpan(new RelativeSizeSpan(1f), 32, 55, 0);
        strPrivacy.setSpan(new StyleSpan(Typeface.BOLD), 32, 55, 0);
        strPrivacy.setSpan(new UnderlineSpan(), 32, 55, 0);
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                displayToast("Terms");
            }
        };
        strPrivacy.setSpan(clickableSpan1, 32, 55, 0);
        strPrivacy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.theme_color_blue)), 32, 55, 0);


        strPrivacy.setSpan(new RelativeSizeSpan(1f), 58, 72, 0);
        strPrivacy.setSpan(new StyleSpan(Typeface.BOLD), 58, 72, 0);
        strPrivacy.setSpan(new UnderlineSpan(), 58, 72, 0);
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                displayToast("Privacy");
            }
        };
        strPrivacy.setSpan(clickableSpan2, 58, 72, 0);
        strPrivacy.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.theme_color_blue)), 58, 72, 0);

        tv_privacyTerms.setMovementMethod(LinkMovementMethod.getInstance());
        tv_privacyTerms.setText(strPrivacy);

    }

    private void init() {
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        et_yourName = (EditText) findViewById(R.id.et_yourName);
        et_businessName = (EditText) findViewById(R.id.et_businessName);
        tv_merchantTypeText = (TextView) findViewById(R.id.tv_merchantTypeText);
        rg_radioGroup = (RadioGroup) findViewById(R.id.rg_radioGroup);
        rb_restaurant = (RadioButton) findViewById(R.id.rb_restaurant);
        rb_retailer = (RadioButton) findViewById(R.id.rb_retailer);
        rb_other = (RadioButton) findViewById(R.id.rb_other);
        et_typeToAdd = (EditText) findViewById(R.id.et_typeToAdd);
        et_emailAddress = (EditText) findViewById(R.id.et_emailAddress);
        et_setPassword = (EditText) findViewById(R.id.et_setPassword);
        et_confirmPassword = (EditText) findViewById(R.id.et_confirmPassword);
        et_phoneNumber = (EditText) findViewById(R.id.et_phoneNumber);
        et_location = (AutoCompleteTextView) findViewById(R.id.et_location);
        tv_operationalHrsText = (TextView) findViewById(R.id.tv_operationalHrsText);
        tv_fromText = (TextView) findViewById(R.id.tv_fromText);
        tv_fromTime = (TextView) findViewById(R.id.tv_fromTime);
        tv_toText = (TextView) findViewById(R.id.tv_toText);
        tv_toTime = (TextView) findViewById(R.id.tv_toTime);
        et_employees = (EditText) findViewById(R.id.et_employees);
        cb_privacyTerms = (CheckBox) findViewById(R.id.cb_privacyTerms);
        tv_privacyTerms = (TextView) findViewById(R.id.tv_privacyTerms);
        tv_registerBtn = (TextView) findViewById(R.id.tv_registerBtn);
        tv_errYourName = (TextView) findViewById(R.id.tv_errYourName);
        tv_errBusinessName = (TextView) findViewById(R.id.tv_errBusinessName);
        tv_errTypeToAdd = (TextView) findViewById(R.id.tv_errTypeToAdd);
        tv_errEmailAddress = (TextView) findViewById(R.id.tv_errEmailAddress);
        tv_errSetPassword = (TextView) findViewById(R.id.tv_errSetPassword);
        tv_errConfirmPassword = (TextView) findViewById(R.id.tv_errConfirmPassword);
        tv_errPhoneNumber = (TextView) findViewById(R.id.tv_errPhoneNumber);
        tv_errLocation = (TextView) findViewById(R.id.tv_errLocation);

        tv_pageTitle.setTypeface(SEMIBOLD);
        et_yourName.setTypeface(SEMIBOLD);
        et_businessName.setTypeface(SEMIBOLD);
        tv_merchantTypeText.setTypeface(REGULAR);
        rb_restaurant.setTypeface(REGULAR);
        rb_retailer.setTypeface(REGULAR);
        rb_other.setTypeface(REGULAR);
        et_typeToAdd.setTypeface(REGULAR);
        et_emailAddress.setTypeface(REGULAR);
        et_setPassword.setTypeface(REGULAR);
        et_confirmPassword.setTypeface(REGULAR);
        et_phoneNumber.setTypeface(REGULAR);
        et_location.setTypeface(REGULAR);
        tv_operationalHrsText.setTypeface(REGULAR);
        tv_fromText.setTypeface(REGULAR);
        tv_fromTime.setTypeface(REGULAR);
        tv_toText.setTypeface(REGULAR);
        tv_toTime.setTypeface(REGULAR);
        et_employees.setTypeface(REGULAR);
        tv_registerBtn.setTypeface(SEMIBOLD);
        tv_errYourName.setTypeface(REGULAR);
        tv_errBusinessName.setTypeface(REGULAR);
        tv_errTypeToAdd.setTypeface(REGULAR);
        tv_errEmailAddress.setTypeface(REGULAR);
        tv_errSetPassword.setTypeface(REGULAR);
        tv_errConfirmPassword.setTypeface(REGULAR);
        tv_errPhoneNumber.setTypeface(REGULAR);
        tv_errLocation.setTypeface(REGULAR);
        cb_privacyTerms.setTypeface(REGULAR);
        tv_privacyTerms.setTypeface(REGULAR);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.iv_backBtn:
                RegistrationActivity.this.finish();
                break;
            case R.id.tv_fromTime:
                BTN_TYPE = 1;
                timePickerDialog();
                break;
            case R.id.tv_toTime:
                BTN_TYPE = 2;
                timePickerDialog();
                break;
            case R.id.tv_registerBtn:
                if (checkEntryValidation()) {
                    if (lat != 0 && lng != 0) {
                        sendRequestRegistration(et_yourName.getText().toString().trim(),
                                et_setPassword.getText().toString().trim()
                                , et_phoneNumber.getText().toString().trim(),
                                et_emailAddress.getText().toString().trim(),
                                et_location.getText().toString().trim(),
                                typeOfMerchant, "",
                                tv_fromTime.getText().toString().trim(),
                                tv_toTime.getText().toString().trim()
                                , et_businessName.getText().toString().trim());
                    } else {
                        showDialog("Alert", "Plz select address from dropdown", "OK");
                    }
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag("Timepickerdialog");

        if (tpd != null) tpd.setOnTimeSetListener(this);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        if (BTN_TYPE == 1)
            updateTime(hourOfDay, minute, tv_fromTime);
        else if (BTN_TYPE == 2)
            updateTime(hourOfDay, minute, tv_toTime);
    }

    private void updateTime(int hours, int minute, TextView timeText) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (minute < 10)
            minutes = "0" + minute;
        else
            minutes = String.valueOf(minute);
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        timeText.setText(aTime);
        timeText.setTextColor(getResources().getColor(R.color.black));
    }

    private void timePickerDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                RegistrationActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.setThemeDark(false);
        tpd.vibrate(false);
        tpd.dismissOnPause(true);
        tpd.enableSeconds(false);
        tpd.setAccentColor(getResources().getColor(R.color.theme_color_blue));
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }

    private boolean checkEntryValidation() {
        if (et_yourName.getText().toString().trim().isEmpty()) {
            tv_errYourName.setText("Plz enter your name");
            tv_errYourName.setVisibility(View.VISIBLE);
            return false;
        } else if (et_businessName.getText().toString().trim().isEmpty()) {
            tv_errBusinessName.setText("Plz enter business name");
            tv_errBusinessName.setVisibility(View.VISIBLE);
            return false;
        } else if (et_emailAddress.getText().toString().trim().isEmpty()) {
            tv_errEmailAddress.setText("Plz enter email address");
            tv_errEmailAddress.setVisibility(View.VISIBLE);
            return false;
        } else if (!et_emailAddress.getText().toString().trim().matches(EMAIL_PATTERN)) {
            tv_errEmailAddress.setText("Plz enter valid email address");
            tv_errEmailAddress.setVisibility(View.VISIBLE);
            return false;
        } else if (et_setPassword.getText().toString().trim().isEmpty()) {
            tv_errSetPassword.setText("Plz enter password");
            tv_errSetPassword.setVisibility(View.VISIBLE);
            return false;
        } else if (et_confirmPassword.getText().toString().trim().isEmpty()) {
            tv_errConfirmPassword.setText("Plz re enter password");
            tv_errConfirmPassword.setVisibility(View.VISIBLE);
            return false;
        } else if (!et_setPassword.getText().toString().trim().equals(et_confirmPassword.getText().toString().trim())) {
            tv_errSetPassword.setVisibility(View.GONE);
            tv_errConfirmPassword.setText("password does not match");
            tv_errConfirmPassword.setVisibility(View.VISIBLE);
            return false;
        } else if (et_phoneNumber.getText().toString().trim().isEmpty()) {
            tv_errPhoneNumber.setText("Plz enter phone number");
            tv_errPhoneNumber.setVisibility(View.VISIBLE);
            return false;
        } else if (et_phoneNumber.getText().toString().trim().length() != 10) {
            tv_errPhoneNumber.setText("Plz enter 10 digits phone number");
            tv_errPhoneNumber.setVisibility(View.VISIBLE);
            return false;
        } else if (et_location.getText().toString().trim().isEmpty()) {
            tv_errLocation.setText("Plz enter location");
            tv_errLocation.setVisibility(View.VISIBLE);
            return false;
        } else if (rg_radioGroup.getCheckedRadioButtonId() == -1) {
            showDialog("Alert", "Plz select or enter merchant type", "OK");
            return false;
        } else if (rg_radioGroup.getCheckedRadioButtonId() == R.id.rb_other && et_typeToAdd.getText().toString().trim().isEmpty()) {
            tv_errTypeToAdd.setText("Plz enter merchant type");
            tv_errTypeToAdd.setVisibility(View.VISIBLE);
            return false;
        } else if (tv_fromTime.getText().toString().equals(getResources().getString(R.string.hint_select_time))) {
            showDialog("Alert", "Plz select open/close time", "OK");
            return false;
        } else if (tv_toTime.getText().toString().equals(getResources().getString(R.string.hint_select_time))) {
            showDialog("Alert", "Plz select open/close time", "OK");
            return false;
        } else {
            typeOfMerchant = ((RadioButton) findViewById(rg_radioGroup.getCheckedRadioButtonId())).getText().toString();
            if (typeOfMerchant.equalsIgnoreCase(getResources().getString(R.string.rb_txt_other))) {
                typeOfMerchant = et_typeToAdd.getText().toString().trim();
            }
            tv_errYourName.setVisibility(View.GONE);
            tv_errBusinessName.setVisibility(View.GONE);
            tv_errEmailAddress.setVisibility(View.GONE);
            tv_errSetPassword.setVisibility(View.GONE);
            tv_errConfirmPassword.setVisibility(View.GONE);
            tv_errPhoneNumber.setVisibility(View.GONE);
            tv_errLocation.setVisibility(View.GONE);
            return true;
        }
    }

    private void sendRequestRegistration(String name, String password, String mobile, String email, String address, String typeOfBusiness, String referralCode, String openTime, String closeTime, String businessName) {
        if (isNetworkAvailable()) {
            new RegistrationAsync().execute(name, password, mobile, email, address, typeOfBusiness, referralCode, openTime, closeTime, businessName);
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            buttonView.setTextColor(getResources().getColor(R.color.black));
        else
            buttonView.setTextColor(getResources().getColor(R.color.light_grey));

        if (buttonView.getId() != R.id.rb_other) {
            et_typeToAdd.setText("");
            tv_errTypeToAdd.setVisibility(View.GONE);
        }
    }

    private class RegistrationAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Registering...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d(TAG, "response : " + result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        displayToast(msg);
                        String token = response.getString("token");
                        String merchantId = response.getString("Merchantid");
                        AppPreferences.setAccessToken(RegistrationActivity.this, token);
                        AppPreferences.setMerchantId(RegistrationActivity.this, merchantId);
                        Intent intent = new Intent(RegistrationActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        RegistrationActivity.this.finish();
                    } else {
                        showDialog("Alert", msg, "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                showDialog("Error", "oh! something went wrong ", "OK");
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.registrationUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
//            httpPost.setHeader("Content-Type", "application/json");
            JSONObject jsonObject = new JSONObject();

            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("name", params[0]));
                nameValuePairs.add(new BasicNameValuePair("password", params[1]));
                nameValuePairs.add(new BasicNameValuePair("mobile", params[2]));
                nameValuePairs.add(new BasicNameValuePair("email", params[3]));
                nameValuePairs.add(new BasicNameValuePair("address", params[4]));
                nameValuePairs.add(new BasicNameValuePair("typeOfBusiness", params[5]));
                nameValuePairs.add(new BasicNameValuePair("referralCode", params[6]));
                nameValuePairs.add(new BasicNameValuePair("openTime", params[7]));
                nameValuePairs.add(new BasicNameValuePair("closeTime", params[8]));
                nameValuePairs.add(new BasicNameValuePair("businessname", params[9]));
                nameValuePairs.add(new BasicNameValuePair("latitude", String.valueOf(lat)));
                nameValuePairs.add(new BasicNameValuePair("longitude", String.valueOf(lng)));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.et_yourName:
                    if (text.length() > 0)
                        tv_errYourName.setVisibility(View.GONE);
                    else {
                        tv_errYourName.setVisibility(View.VISIBLE);
                        tv_errYourName.setText("Plz enter your name");
                    }
                    break;
                case R.id.et_businessName:
                    if (text.length() > 0)
                        tv_errBusinessName.setVisibility(View.GONE);
                    else {
                        tv_errBusinessName.setVisibility(View.VISIBLE);
                        tv_errBusinessName.setText("Plz enter business name");
                    }
                    break;
                case R.id.et_typeToAdd:
                    if (text.length() > 0) {
                        rb_other.setChecked(true);
                        rb_other.setTextColor(getResources().getColor(R.color.black));
                        tv_errTypeToAdd.setVisibility(View.GONE);
                        typeOfMerchant = text;
                    } else {
                        tv_errTypeToAdd.setVisibility(View.VISIBLE);
                        tv_errTypeToAdd.setText("Plz tell us which type of merchant you are?");
                    }
                    break;
                case R.id.et_emailAddress:
                    if (text.length() > 0) {
                        if (text.matches(EMAIL_PATTERN))
                            tv_errEmailAddress.setVisibility(View.GONE);
                        else {
                            tv_errEmailAddress.setVisibility(View.VISIBLE);
                            tv_errEmailAddress.setText("Plz enter valid email address");
                        }
                    } else {
                        tv_errEmailAddress.setVisibility(View.VISIBLE);
                        tv_errEmailAddress.setText("Plz enter email address");
                    }
                    break;
                case R.id.et_setPassword:
                    if (text.length() > 0)
                        tv_errSetPassword.setVisibility(View.GONE);
                    else {
                        tv_errSetPassword.setText("Plz enter password");
                        tv_errSetPassword.setVisibility(View.VISIBLE);
                    }
                    break;
                case R.id.et_confirmPassword:
                    if (text.length() > 0)
                        if (text.equals(et_setPassword.getText().toString().trim()))
                            tv_errConfirmPassword.setVisibility(View.GONE);
                        else {
                            tv_errConfirmPassword.setVisibility(View.VISIBLE);
                            tv_errConfirmPassword.setText("Entered password does not match");
                        }
                    else {
                        tv_errConfirmPassword.setText("Plz re enter password");
                        tv_errConfirmPassword.setVisibility(View.VISIBLE);
                    }
                    break;
                case R.id.et_phoneNumber:
                    if (text.length() > 0)
                        if (text.length() != 10) {
                            tv_errPhoneNumber.setVisibility(View.VISIBLE);
                            tv_errPhoneNumber.setText("Plz enter 10 digits valid phone number");
                        } else
                            tv_errPhoneNumber.setVisibility(View.GONE);
                    else {
                        tv_errPhoneNumber.setVisibility(View.VISIBLE);
                        tv_errPhoneNumber.setText("Plz enter phone number");
                    }
                    break;
                case R.id.et_location:
                    sendRequestLocationSearch(text);
                    if (text.length() > 0)
                        tv_errLocation.setVisibility(View.GONE);
                    else {
                        lat = 0;
                        lng = 0;
                        tv_errLocation.setVisibility(View.VISIBLE);
                        tv_errLocation.setText("Plz enter location");
                    }
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    private void sendRequestLocationSearch(String text) {
        if (isNetworkAvailable()) {
            new LocationSearchAsync().execute(text);
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }
    }


    private class LocationSearchAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            addressDataList = new ArrayList<>();
            ArrayList<String> addressList = new ArrayList<>();
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    String status = response.getString("status");
                    if (status.equals("OK")) {
                        JSONArray dataList = response.getJSONArray("predictions");
                        if (dataList != null && dataList.length() > 0) {
                            for (int i = 0; i < dataList.length(); i++) {
                                rowData = new GPSAddressListItems();
                                JSONObject dataObj = dataList.getJSONObject(i);
                                String _description = dataObj.getString("description");
                                String _place_id = dataObj.getString("place_id");
                                StringTokenizer st = new StringTokenizer(_description, ",");
                                String _areaName = st.nextToken().toString();

                                rowData.setAreaName(_areaName);
                                rowData.setAddress(_description);
                                rowData.setPlaceId(_place_id);
//                                if (_description.contains("Maharashtra"))
                                addressDataList.add(rowData);
                                addressList.add(_description);
                            }
                        }
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            ArrayAdapter<String> actvAdapter = new ArrayAdapter<String>(RegistrationActivity.this, R.layout.row_gps_address_list, addressList);
            et_location.setAdapter(actvAdapter);
        }

        @Override
        protected String doInBackground(String... params) {
            String data = "";
            try {
                data = downloadUrl(CreateUrl.searchPlaceUrl(params[0]));
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void sendPlaceDetailRequest(String placeId) {
        if (isNetworkAvailable()) {
            if (placeId.length() > 0) {
                new PlacesDetailTask().execute(placeId);
            } else {
                displayToast("Place id missing");
            }
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }
    }

    private class PlacesDetailTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait...", false);
        }

        @Override
        protected String doInBackground(String... placeId) {
            String data = "";
            try {
                data = downloadUrl(CreateUrl.getPlaceDetailUrl(placeId[0]));
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    String status = response.getString("status");
                    if (status.equals("OK")) {
                        JSONObject resultObj = response.getJSONObject("result");
                        JSONObject geometryObj = resultObj.getJSONObject("geometry");
                        JSONObject latLongObj = geometryObj.getJSONObject("location");
                        lat = latLongObj.getDouble("lat");
                        lng = latLongObj.getDouble("lng");
                        Log.d(TAG, "LAT : " + lat + " , LNG : " + lng);
                    } else {
                        showDialog("Error", "Something went wrong", "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }
    }
}
