package com.android.aircountr;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.aircountr.fragment.ExpenseFragment;
import com.android.aircountr.fragment.PredictorFragment;
import com.android.aircountr.fragment.VendorsListFragment;
import com.android.aircountr.objects.CategoryListItem;

import java.util.ArrayList;
import java.util.List;

import io.karim.MaterialTabs;

/**
 * Created by gaura on 5/21/2016.
 */
public class ExpenseActivity extends FragmentActivity {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private ImageView iv_export;
    private MaterialTabs tabs;
    private ViewPager viewPager;
    public MaterialDialog processing;
    public Typeface REGULAR, SEMIBOLD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);

        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_pageTitle.setTypeface(SEMIBOLD);
        iv_export = (ImageView) findViewById(R.id.iv_export);
        tabs = (MaterialTabs) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        ArrayList<String> tabsList = new ArrayList<>();
        tabsList.add("Expense");
        tabsList.add("Predictor");
        setupViewPager(viewPager, tabsList);

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExpenseActivity.this.finish();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<String> mDataList) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mDataList);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager, List<String> mFragmentTitleList) {
            super(manager);
            this.mFragmentTitleList = mFragmentTitleList;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new ExpenseFragment();
                    break;
                case 1:
                    fragment = new PredictorFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public void showDialog(String title, String msg, String btnText) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(msg)
                .backgroundColorRes(R.color.white)
                .titleColorRes(R.color.black)
                .contentColor(Color.BLACK)
                .positiveColorRes(R.color.theme_color_blue)
                .buttonRippleColorRes(R.color.transparent_theme_color)
                .positiveText(btnText)
                .cancelable(false)
                .show();
    }

    public void showLoading(String message, boolean cancelable) {
        processing = new MaterialDialog.Builder(this)
                //.title(R.string.progress_dialog)
                .content(message)
                .progress(true, 0)
                .backgroundColorRes(R.color.white)
                .contentColor(getResources().getColor(R.color.black))
                .cancelable(cancelable)
                .show();
    }

    public void hideLoading() {
        if (processing != null)
            processing.dismiss();
    }
}
