package com.android.aircountr.objects;

/**
 * Created by gaurav on 5/3/2016.
 */
public class CategoryIconsListItem {
    int resourceId;
    boolean isSelected;

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
