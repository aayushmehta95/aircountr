package com.android.aircountr.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.R;
import com.android.aircountr.objects.InvoiceListItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by gaura on 6/2/2016.
 */
public class InvoiceListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<InvoiceListItem> mDataList;
    private Typeface REGULAR, SEMIBOLD;

    public InvoiceListAdapter(Context mContext, ArrayList<InvoiceListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Semibold.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<InvoiceListItem> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.row_invoice_list, null);

        ImageView iv_invoiceImg = (ImageView) convertView.findViewById(R.id.iv_invoiceImg);
        TextView tv_vendorName = (TextView) convertView.findViewById(R.id.tv_vendorName);
        TextView tv_categoryName = (TextView) convertView.findViewById(R.id.tv_categoryName);
        TextView tv_comment = (TextView) convertView.findViewById(R.id.tv_comment);
        TextView tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);

        tv_vendorName.setTypeface(SEMIBOLD);
        tv_categoryName.setTypeface(SEMIBOLD);
        tv_comment.setTypeface(REGULAR);
        tv_amount.setTypeface(SEMIBOLD);

        tv_vendorName.setText(mDataList.get(position).getVendorName());
        tv_categoryName.setText(mDataList.get(position).getCategoryName());
        tv_comment.setText(mDataList.get(position).getComment());
        tv_amount.setText(mContext.getResources().getString(R.string.txt_rupee) + mDataList.get(position).getInvoiceAmount());

        if (mDataList.get(position).getImageUrl() != null && !mDataList.get(position).getImageUrl().equals("")) {
            Picasso.with(mContext).load(mDataList.get(position).getImageUrl()).into(iv_invoiceImg);
        } else iv_invoiceImg.setImageResource(R.drawable.ic_logo);

        return convertView;
    }
}
