package com.android.aircountr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.android.aircountr.R;
import com.android.aircountr.objects.CategoryIconsListItem;

import java.util.ArrayList;


/**
 * Created by gaurav on 4/27/2016.
 */
public class AddCategoryIconsListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<CategoryIconsListItem> mDataList;

    public AddCategoryIconsListAdapter(Context mContext, ArrayList<CategoryIconsListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<CategoryIconsListItem> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.column_category_icons_grid, null);
            viewHolder.iconImg = (ImageView) convertView.findViewById(R.id.iv_iconImg);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.iconImg.setImageResource(mDataList.get(position).getResourceId());
        if (mDataList.get(position).isSelected()) {
            convertView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.shape_rect_blue_border_with_transparent_bg));
        } else {
            convertView.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.shape_rect_blue_border_category));
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView iconImg;
    }
}
