package com.android.aircountr;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.prefrences.AppPreferences;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by gaurav on 4/10/2016.
 */
public class SignInActivity extends BaseActivity implements View.OnClickListener {
    private String TAG = this.getClass().getSimpleName();
    private EditText et_email;
    private EditText et_password;
    private TextView tv_forgotPassword;
    private TextView tv_signIn;
    private TextView tv_or;
    private TextView tv_newUser;
    private TextView tv_signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        tv_forgotPassword = (TextView) findViewById(R.id.tv_forgotPassword);
        tv_signIn = (TextView) findViewById(R.id.tv_signIn);
        tv_or = (TextView) findViewById(R.id.tv_or);
        tv_newUser = (TextView) findViewById(R.id.tv_newUser);
        tv_signUp = (TextView) findViewById(R.id.tv_signUp);

        et_email.setTypeface(REGULAR);
        et_password.setTypeface(REGULAR);
        tv_forgotPassword.setTypeface(REGULAR);
        tv_signIn.setTypeface(REGULAR);
        tv_or.setTypeface(REGULAR);
        tv_newUser.setTypeface(REGULAR);
        tv_signUp.setTypeface(REGULAR);

        tv_signUp.setOnClickListener(this);
        tv_signIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.tv_signUp: {
                switchActivity(SignInActivity.this, RegistrationActivity.class);
            }
            break;
            case R.id.tv_signIn:
                if (AppPreferences.getMerchantId(SignInActivity.this).equals("") && AppPreferences.getAccessToken(SignInActivity.this).equals("")) {
                    authenticateUser(et_email.getText().toString(), et_password.getText().toString());
                } else {
                    switchActivity(SignInActivity.this, HomeActivity.class);
                }
                break;
        }
    }

    private void authenticateUser(final String email, final String password) {
        if (isNetworkAvailable()) {
            // Tag used to cancel the request
            String tag_json_obj = "json_obj_authenticate";
            final String url = CreateUrl.getLoginUrl();
            Map<String, String> params = new HashMap<String, String>();
            params.put("mobile", email);
            params.put("password", password);

            final ProgressDialog progessDialog = new ProgressDialog(this);
            progessDialog.setMessage("Authenticating...");
            progessDialog.show();

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, response.toString());
                    try {
                        Boolean status = response.getBoolean("success");
                        if (status) {
                            AppPreferences.setAccessToken(SignInActivity.this, response.getString("token"));
                            AppPreferences.setMerchantId(SignInActivity.this, response.getString("Merchantid"));
                            switchActivity(SignInActivity.this, HomeActivity.class);
                        } else {
                            displayToast("Please Register");
                            switchActivity(SignInActivity.this, RegistrationActivity.class);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progessDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // hide the progress dialog
                    progessDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile", email);
                    params.put("password", password);
                    return params;
                }

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            // Adding request to request queue
            AircountrApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }
    }
}