package com.android.aircountr;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by gaura on 5/23/2016.
 */
public class BillDetailAutoActivity extends BaseActivity implements UrlConstants {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private ImageView iv_imgBtn;
    private EditText et_comment;
    private TextView tv_saveBtn;
    private final int CAMERA_REQUEST = 101, GALLERY_REQUEST = 100;
    private byte[] image_byte;
    private ByteArrayBody bab;
    private String categoryId = "", categoryName = "";
    private Uri fileUri;
    private static final String IMAGE_DIRECTORY_NAME = "Aircountr";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail_auto);

        Intent intent = getIntent();
        categoryId = intent.getStringExtra(CATEGORY_ID);
        categoryName = intent.getStringExtra(CATEGORY_NAME);

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        iv_imgBtn = (ImageView) findViewById(R.id.iv_imgBtn);
        et_comment = (EditText) findViewById(R.id.et_comment);
        tv_saveBtn = (TextView) findViewById(R.id.tv_saveBtn);

        tv_pageTitle.setTypeface(SEMIBOLD);
        et_comment.setTypeface(REGULAR);
        tv_saveBtn.setTypeface(SEMIBOLD);

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillDetailAutoActivity.this.finish();
            }
        });

        iv_imgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImgOptionPopUp();
            }
        });

        tv_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bab != null) {
                    sendBillDetailRequest(categoryId, et_comment.getText().toString());
                } else {
                    showDialog("Alert", "Plz select image", "OK");
                }
            }
        });

//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private void sendBillDetailRequest(String categoryId, String comment) {
        if (!AppPreferences.getMerchantId(BillDetailAutoActivity.this).equals("") && !AppPreferences.getAccessToken(BillDetailAutoActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new BillDetailAsync().execute(categoryId, comment);
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            switchActivity(BillDetailAutoActivity.this, SignInActivity.class);
        }
    }

    private class BillDetailAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        displayToast(msg);
                        BillDetailAutoActivity.this.finish();
                    } else {
                        showDialog("Aler", msg, "OK");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.uploadBillDetail();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(BillDetailAutoActivity.this));
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            try {
                entity.addPart("merchantId", new StringBody(AppPreferences.getMerchantId(BillDetailAutoActivity.this)));
                entity.addPart("categoryId", new StringBody(params[0]));
                entity.addPart("isautomode", new StringBody("true"));
                entity.addPart("comments", new StringBody(params[1]));
                entity.addPart("categoryname", new StringBody(categoryName));
                entity.addPart("isprocessed", new StringBody("false"));
                entity.addPart("invoice", bab);
                ;

                httpPost.setEntity(entity);
                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "Response : " + _response);
            return _response;
        }
    }

    private void selectImgOptionPopUp() {
        final Dialog dialog = new Dialog(BillDetailAutoActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_select_img_layout);
        TextView tv_cameraBtn = (TextView) dialog.findViewById(R.id.tv_cameraBtn);
        TextView tv_galleryBtn = (TextView) dialog.findViewById(R.id.tv_galleryBtn);
        TextView tv_cancelBtn = (TextView) dialog.findViewById(R.id.tv_cancelBtn);

        tv_cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(intent, CAMERA_REQUEST);
                captureImage();
                dialog.dismiss();
            }
        });

        tv_galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                dialog.dismiss();
            }
        });

        tv_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        Uri selectedImage = data.getData();
                        iv_imgBtn.setImageURI(selectedImage);
                        try {
                            FileInputStream imageStream = (FileInputStream) getContentResolver().openInputStream(selectedImage);
                            Bitmap selectedImg = BitmapFactory.decodeStream(imageStream);
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            selectedImg.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
                            image_byte = outputStream.toByteArray();
                            bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".jpg");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        displayToast("Selected image not found");
                    }
                }
                break;
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
//                    iv_imgBtn.setImageBitmap(photo);
//                    Uri tempUri = getImageUri(getApplicationContext(), photo);
//                    File finalFile = new File(getRealPathFromURI(tempUri));
//                    Log.d(TAG, "A PATH : " + finalFile.getAbsolutePath());
//                    Log.d(TAG, "B PATH : " + finalFile.getPath());
//                    Bitmap selectedImg = BitmapFactory.decodeFile(finalFile.getAbsolutePath());
//                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//                    selectedImg.compress(Bitmap.CompressFormat.PNG, 70, outputStream);
//                    image_byte = outputStream.toByteArray();
//                    bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".jpg");

                    try {
                        Log.d(TAG,"uri :"+fileUri.getPath());
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 1;
                        final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);
                        iv_imgBtn.setImageBitmap(bitmap);
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 70, outputStream);
                        image_byte = outputStream.toByteArray();
                        bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".png");

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(1);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        }  else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("u", fileUri.toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
       fileUri = Uri.parse(savedInstanceState.getString("u"));
    }
}
