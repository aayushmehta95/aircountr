package com.android.aircountr.constants;

/**
 * Created by gaurav on 4/30/2016.
 */
public interface UrlConstants {
    public static String BASE_URL = "http://api.aircountr.com/api/";
    public static String REGISTRATION_URL = "signup";
    public static String GET_CATEGORIES_URL = "categories/";
    public static String DELETE_CATEGORIES_URL = "deletecategory/";
    public static String UPLOAD_BILL_DETAIL_URL = "createInvoice/";
    public static String ADD_VENDORS_URL = "vendors/";
    public static String GET_CALENDAR_URL = "fetchcalendardata/";
    public static String EXPORT_REPORT_URL = "sendexportemail";
    public static String EXPENSE_CHART_URL = "fetchvendorpercentage/";
    public static String INVOICE_LIST_URL = "invoices/";
    public static String WALLET_URL = "getwalletbal/";
    public static String FORGOT_PASS_URL = "forgot";
    public static String LOGIN_URL = "authenticate";

    /**
     * Google place search and place details urls
     */
    public static String PLACE_API_KEY = "AIzaSyBHXxbnS8ZwS2WfDXcz9VQDSOuXpchDrFU";
    public static String PLACE_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/";
    public static String PLACE_DETAIL_URL = "https://maps.googleapis.com/maps/api/place/details/json?";

    public static String CATEGORY_ID = "category-id";
    public static String CATEGORY_NAME = "category-name";
    public static String TIMESTAMP = "timestamp";
    public static String DAY_OF_MONTH = "day-of-month";
}