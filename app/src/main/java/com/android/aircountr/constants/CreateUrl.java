package com.android.aircountr.constants;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by gaurav on 4/30/2016.
 */
public class CreateUrl implements UrlConstants {

    public static String registrationUrl() {
        String url = "";
        url = BASE_URL + REGISTRATION_URL;
        return url;
    }

    public static String getCategoriesUrl(String merchantId) {
        String url = "";
        url = BASE_URL + GET_CATEGORIES_URL + merchantId;
        return url;
    }

    public static String addCategoriesUrl() {
        String url = "";
        url = BASE_URL + GET_CATEGORIES_URL;
        return url;
    }

    public static String deleteCategory() {
        String url = "";
        url = BASE_URL + DELETE_CATEGORIES_URL;
        return url;
    }

    public static String uploadBillDetail() {
        String url = "";
        url = BASE_URL + UPLOAD_BILL_DETAIL_URL;
        return url;
    }

    public static String addVendorUrl() {
        String url = "";
        url = BASE_URL + ADD_VENDORS_URL;
        return url;
    }

    public static String getCalendarDataUrl(String merchantId, String timeStamp, String
            vendorId, String categoryId) {
        String url = "";
        if (categoryId.equals("") && vendorId.equals("")) {
            if (categoryId.equals("") && vendorId.equals("")) {
                url = BASE_URL + GET_CALENDAR_URL + merchantId + "/" + timeStamp;
            } else if (!categoryId.equals("") && vendorId.equals("")) {
                url = BASE_URL + GET_CALENDAR_URL + merchantId + "/" + timeStamp + "?categoryId=" + categoryId;
            } else if (!categoryId.equals("")) {
                url = BASE_URL + GET_CALENDAR_URL + merchantId + "/" + timeStamp + "?categoryId=" + categoryId + "&vendorId=" + vendorId;
            }
        }
        Log.d("URL", "URL : " + url);
        return url;
    }

    public static String forgotPasswordUrl() {
        String url = "";
        url = BASE_URL + FORGOT_PASS_URL;
        return url;
    }

    public static String exportReportsUrl() {
        String url = "";
        url = BASE_URL + EXPORT_REPORT_URL;
        return url;
    }

    public static String expenseChartUrl(String merchantId, String timeStamp) {
        String url = "";
        url = BASE_URL + EXPENSE_CHART_URL + merchantId + "/" + timeStamp;
        return url;
    }

    public static String invoiceListUrl(String merchantId, String timeStamp, String day) {
        String url = "";
        url = BASE_URL + INVOICE_LIST_URL + merchantId + "/" + timeStamp + "/" + day;
        return url;
    }

    public static String getWalletUrl(String merchantId) {
        String url = "";
        url = BASE_URL + WALLET_URL + merchantId;
        return url;
    }

    /**
     * Google place search and place details urls
     *
     * @param strSearch
     * @return
     */

    public static String searchPlaceUrl(String strSearch) {
        String url = "";
        String key = "key=" + PLACE_API_KEY;
        String input = "";
        try {
            input = "input=" + URLEncoder.encode(strSearch, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String types = "types=geocode";
        String sensor = "sensor=true";
        String parameters = input + "&" + types + "&" + sensor + "&" + key;
        String output = "json";
        url = PLACE_SEARCH_URL + output + "?" + parameters;
        return url;
    }

    public static String getPlaceDetailUrl(String strPlaceId) {
        String url = "";
        String key = "key=" + PLACE_API_KEY;
        String input = "";

        try {
            input = "placeid=" + URLEncoder.encode(strPlaceId, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        url = PLACE_DETAIL_URL + input + "&" + key;
        return url;
    }

    public static String getLoginUrl() {
        String url = "";
        url = BASE_URL + LOGIN_URL;
        return url;
    }
}