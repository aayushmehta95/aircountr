package com.android.aircountr.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.aircountr.AddVendorActivity;
import com.android.aircountr.MyVendorsActivity;
import com.android.aircountr.R;
import com.android.aircountr.adapters.VendorsListAdapter;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.objects.CategoryListItem;

import java.util.ArrayList;


/**
 * Created by gaurav on 4/29/2016.
 */
public class VendorsListFragment extends Fragment implements UrlConstants, VendorsListAdapter.VendorsEventClickListener {

    private ListView lv_vendors;
    private TextView tv_msg;
    private ImageView iv_addVendorBtn;
    private TextView tv_addVendorText;
    public ArrayList<CategoryListItem.Vendors> mVendorsList;
    public String categoryId = "", categoryName = "";
    private VendorsListAdapter mVendorsListAdapter;
    private static final int PERMISSIONS_REQUEST_CALL_PHONE = 201;
    private String phoneNumber = "";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tv_msg.setTypeface(((MyVendorsActivity) getActivity()).SEMIBOLD);
        tv_addVendorText.setTypeface(((MyVendorsActivity) getActivity()).REGULAR);

        if (null != mVendorsList && mVendorsList.size() > 0) {
            tv_msg.setVisibility(View.GONE);
            mVendorsListAdapter = new VendorsListAdapter(getActivity(), mVendorsList);
            lv_vendors.setAdapter(mVendorsListAdapter);
            mVendorsListAdapter.setVendorClickEventListener(this);
            mVendorsListAdapter.onDataSetChanged(mVendorsList);
        } else {
            tv_msg.setVisibility(View.VISIBLE);
            tv_msg.setText("No vendor found.");
        }

        iv_addVendorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!categoryId.equals("") && !categoryName.equals("")) {
                    Intent intent = new Intent(getActivity(), AddVendorActivity.class);
                    intent.putExtra(CATEGORY_ID, categoryId);
                    intent.putExtra(CATEGORY_NAME, categoryName);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Toast.makeText(getActivity(), "Category missing", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_vendors_list, null);
        lv_vendors = (ListView) root.findViewById(R.id.lv_vendors);
        tv_msg = (TextView) root.findViewById(R.id.tv_msg);
        iv_addVendorBtn = (ImageView) root.findViewById(R.id.iv_addVendorBtn);
        tv_addVendorText = (TextView) root.findViewById(R.id.tv_addVendorText);
        return root;
    }

    @Override
    public void onEventClickListener(int btnType, String phoneNumber) {
        this.phoneNumber = phoneNumber;
        if (btnType == 1) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CALL_PHONE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_CALL_PHONE);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                startActivity(intent);
            }
        } else if (btnType == 2) {
            Uri uri = Uri.parse("smsto:" + phoneNumber);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.setPackage("com.whatsapp");
            startActivity(Intent.createChooser(i, ""));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                    startActivity(intent);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}
