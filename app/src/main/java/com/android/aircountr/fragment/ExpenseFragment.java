package com.android.aircountr.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.aircountr.ExpenseActivity;
import com.android.aircountr.R;
import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.objects.ExpenseChartDataItem;
import com.android.aircountr.prefrences.AppPreferences;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by gaura on 5/21/2016.
 */
public class ExpenseFragment extends Fragment {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_previousDateBtn;
    private TextView tv_selectedDate;
    private ImageView iv_nextDateBtn;
    private PieChart mChart;
    private Calendar cal;
    private String strTimeStamp = null;
    private ArrayList<ExpenseChartDataItem> expenseChartDataList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_expense, null);
        iv_previousDateBtn = (ImageView) root.findViewById(R.id.iv_previousDateBtn);
        tv_selectedDate = (TextView) root.findViewById(R.id.tv_selectedDate);
        iv_nextDateBtn = (ImageView) root.findViewById(R.id.iv_nextDateBtn);
        mChart = (PieChart) root.findViewById(R.id.pc_expense);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tv_selectedDate.setTypeface(((ExpenseActivity) getActivity()).SEMIBOLD);

        expenseChartDataList = new ArrayList<>();

        cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
        strTimeStamp = String.valueOf(cal.getTimeInMillis());

        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

//        tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        mChart.setCenterTextTypeface(((ExpenseActivity) getActivity()).REGULAR);
//        mChart.setCenterText(generateCenterSpannableText());

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
//        mChart.setOnChartValueSelectedListener(this);

        sendExpenseChartRequest();

        iv_previousDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, -1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                sendExpenseChartRequest();
            }
        });

        iv_nextDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, 1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                sendExpenseChartRequest();
            }
        });
    }

    private void setData() {
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        for (int i = 0; i < expenseChartDataList.size(); i++) {
            yVals1.add(new Entry((float) (Math.random() * expenseChartDataList.get(i).getPercentage()) + (expenseChartDataList.get(i).getPercentage() / 5), i));
        }

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < expenseChartDataList.size(); i++)
            xVals.add(expenseChartDataList.get(i).getName().toUpperCase());

        PieDataSet dataSet = new PieDataSet(yVals1, "Expense Results");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(((ExpenseActivity) getActivity()).REGULAR);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

    private void sendExpenseChartRequest() {
        if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
            if (((ExpenseActivity) getActivity()).isNetworkAvailable()) {
                new ExpenseChartAsync().execute(strTimeStamp);
            } else {
                ((ExpenseActivity) getActivity()).showDialog("Error", getActivity().getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            ((ExpenseActivity) getActivity()).showDialog("Alert", "Oh! you seems logged out", "OK");
        }
    }

    private class ExpenseChartAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((ExpenseActivity) getActivity()).showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "Response :" + s);
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    JSONArray dataArray = response.getJSONArray("data");
                    if (dataArray != null && dataArray.length() > 0) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            ExpenseChartDataItem rowData = new ExpenseChartDataItem();
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            String _id = dataObj.getString("_id");
                            String totalInvoice = dataObj.getString("totalinvoice");
                            int percentage = dataObj.getInt("percentage");

                            rowData.setName(_id);
                            rowData.setAmount(totalInvoice);
                            rowData.setPercentage(percentage);
                            expenseChartDataList.add(rowData);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setData();

                mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
                // mChart.spin(2000, 0, 360);

                Legend l = mChart.getLegend();
                l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
                l.setXEntrySpace(7f);
                l.setYEntrySpace(0f);
                l.setYOffset(0f);
            }
            ((ExpenseActivity) getActivity()).hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.expenseChartUrl(AppPreferences.getMerchantId(getActivity()), params[0]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(getActivity()));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
}
