package com.android.aircountr.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.aircountr.AddCategoryActivity;
import com.android.aircountr.AircountrApplication;
import com.android.aircountr.BillDetailAutoActivity;
import com.android.aircountr.BillDetailsActivity;
import com.android.aircountr.HomeActivity;
import com.android.aircountr.R;
import com.android.aircountr.SignInActivity;
import com.android.aircountr.adapters.CategoryListAdapter;
import com.android.aircountr.constants.CreateUrl;
import com.android.aircountr.constants.UrlConstants;
import com.android.aircountr.objects.CategoryListItem;
import com.android.aircountr.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by gaurav on 4/26/2016.
 */
public class HomeFragment extends Fragment implements UrlConstants {

    private String TAG = this.getClass().getSimpleName();
    private TextView tv_headingText;
    private GridView gv_categoryList;
    private CategoryListAdapter mCategoryListAdapter;
    private CategoryListItem rowData;
    private ImageView iv_addCategoryBtn;
    private TextView tv_addCategoryText;
    private TextView tv_emptyListMsg;
    private int deletedItemPosition = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, null);

        tv_headingText = (TextView) root.findViewById(R.id.tv_headingText);
        gv_categoryList = (GridView) root.findViewById(R.id.gv_categoryList);
        iv_addCategoryBtn = (ImageView) root.findViewById(R.id.iv_addCategoryBtn);
        tv_addCategoryText = (TextView) root.findViewById(R.id.tv_addCategoryText);
        tv_emptyListMsg = (TextView) root.findViewById(R.id.tv_emptyListMsg);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tv_headingText.setTypeface(((HomeActivity) getActivity()).SEMIBOLD);
        tv_emptyListMsg.setTypeface(((HomeActivity) getActivity()).SEMIBOLD);
        tv_addCategoryText.setTypeface(((HomeActivity) getActivity()).REGULAR);

//        mCategoryList = new ArrayList<>();
        ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems = new ArrayList<>();
        mCategoryListAdapter = new CategoryListAdapter(getActivity(), ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems);
        gv_categoryList.setAdapter(mCategoryListAdapter);

        iv_addCategoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddCategoryActivity.class);
                startActivity(intent);
            }
        });

        gv_categoryList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                sendRequestDeleteCategory(((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryId());
                deletedItemPosition = position;
                return true;
            }
        });

        gv_categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectOptionPopUp(position);
            }
        });

        ((HomeActivity) getActivity()).et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strText = s.toString();
                if (strText.length() > 0) {
                    ArrayList<CategoryListItem> searchList = new ArrayList<CategoryListItem>();
                    for (int i = 0; i < ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.size(); i++) {
                        if (((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(i).getCategoryName().toLowerCase().startsWith(strText.toLowerCase())) {
                            rowData = new CategoryListItem();
                            rowData = ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(i);
                            searchList.add(rowData);
                        }
                    }
                    if (searchList.size() > 0) {
                        mCategoryListAdapter.onDataSetChanged(searchList);
                        tv_emptyListMsg.setVisibility(View.GONE);
                    } else {
                        tv_emptyListMsg.setVisibility(View.VISIBLE);
                        tv_emptyListMsg.setText("no result found");
                    }
                } else {
                    mCategoryListAdapter.onDataSetChanged(((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems);
                    tv_emptyListMsg.setVisibility(View.GONE);
                }
            }
        });
    }

    private void sendRequestGetCategory() {
        if (((HomeActivity) getActivity()).isNetworkAvailable()) {
            if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
                new GetCategoriesAsync().execute(AppPreferences.getMerchantId(getActivity()), AppPreferences.getAccessToken(getActivity()));
            } else {
//                ((HomeActivity) getActivity()).showDialog("Error", "Oh! something went wrong", "OK");
                ((HomeActivity) getActivity()).switchActivity(getActivity(), SignInActivity.class);
            }
        } else {
            ((HomeActivity) getActivity()).showDialog("Error", getActivity().getResources().getString(R.string.no_internet), "OK");
        }
    }

    private class GetCategoriesAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //((HomeActivity) getActivity()).showLoading("Loading...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.clear();
            if (result != null) {
                Log.d(TAG, "Response : " + result);
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray categoryArray = response.getJSONArray("category");
                        if (categoryArray != null && categoryArray.length() > 0) {
                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject categoryData = categoryArray.getJSONObject(i);
                                rowData = new CategoryListItem();
                                String categoryId = categoryData.getString("_id");
                                String resourceId = categoryData.getString("resourceid");
                                String merchantId = categoryData.getString("merchantId");
                                String categoryName = categoryData.getString("name");
                                boolean isVisible = categoryData.getBoolean("view");
                                JSONArray vendorsArray = categoryData.getJSONArray("vendors");
                                ArrayList<CategoryListItem.Vendors> vendorsList = new ArrayList<>();
                                if (vendorsArray != null && vendorsArray.length() > 0) {
                                    for (int j = 0; j < vendorsArray.length(); j++) {
                                        JSONObject vendor = vendorsArray.getJSONObject(j);
                                        CategoryListItem.Vendors vRowData = rowData.new Vendors();
                                        String vCategoryId = vendor.getString("categoryId");
                                        String vCategory = vendor.getString("category");
                                        String vMerchantId = vendor.getString("merchantId");
                                        String vAddress = vendor.getString("address");
                                        String vNumber = vendor.getString("number");
                                        String vName = vendor.getString("name");

                                        vRowData.setCategoryId(vCategoryId);
                                        vRowData.setCategory(vCategory);
                                        vRowData.setMerchantId(vMerchantId);
                                        vRowData.setVendorAddress(vAddress);
                                        vRowData.setVendorNumber(vNumber);
                                        vRowData.setVendorName(vName);
                                        vendorsList.add(vRowData);
                                    }
                                }
                                rowData.setCategoryId(categoryId);
                                rowData.setResourceId(resourceId);
                                rowData.setMerchantId(merchantId);
                                rowData.setCategoryName(categoryName);
                                rowData.setIsVisible(isVisible);
                                rowData.setVendorsDataList(vendorsList);
                                ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.add(rowData);
                            }
                        }
                    } else {
                        String message = response.getString("msg");
                        ((HomeActivity) getActivity()).showDialog("Alert", message, "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //((HomeActivity) getActivity()).hideLoading();
            mCategoryListAdapter.onDataSetChanged(((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems);
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.getCategoriesUrl(params[0]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", params[1]);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    private void sendRequestDeleteCategory(String categoryId) {
        if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
            if (((HomeActivity) getActivity()).isNetworkAvailable()) {
                new DeleteCategoryAsync().execute(AppPreferences.getAccessToken(getActivity()), AppPreferences.getMerchantId(getActivity()), categoryId);
            } else {
                ((HomeActivity) getActivity()).showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            ((HomeActivity) getActivity()).switchActivity(getActivity(), SignInActivity.class);
        }
    }

    private class DeleteCategoryAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((HomeActivity) getActivity()).showLoading("Deleting...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        ((HomeActivity) getActivity()).displayToast(msg);
                        if (deletedItemPosition != -1)
                            ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.remove(deletedItemPosition);

                    } else {
                        ((HomeActivity) getActivity()).showDialog("Alert", msg, "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            ((HomeActivity) getActivity()).hideLoading();

            mCategoryListAdapter.onDataSetChanged(((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems);
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.deleteCategory();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", params[0]);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("merchantId", params[1]);
                jsonObject.put("categoryId", params[2]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sendRequestGetCategory();
    }

    private void selectOptionPopUp(final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_auto_manual_option_layout);

        TextView title = (TextView) dialog.findViewById(R.id.tv_popUpTitle);
        TextView autoText = (TextView) dialog.findViewById(R.id.tv_autoText);
        TextView manualText = (TextView) dialog.findViewById(R.id.tv_manualText);
        LinearLayout manualBtn = (LinearLayout) dialog.findViewById(R.id.ll_manualBtn);
        LinearLayout autoBtn = (LinearLayout) dialog.findViewById(R.id.ll_autoBtn);
        title.setTypeface(((HomeActivity) getActivity()).SEMIBOLD);
        autoText.setTypeface(((HomeActivity) getActivity()).REGULAR);
        manualText.setTypeface(((HomeActivity) getActivity()).REGULAR);

        manualBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BillDetailsActivity.class);
                intent.putExtra(CATEGORY_ID, ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryId());
                intent.putExtra(CATEGORY_NAME, ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryName());
                startActivity(intent);
                dialog.dismiss();
            }
        });

        autoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BillDetailAutoActivity.class);
                intent.putExtra(CATEGORY_ID, ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryId());
                intent.putExtra(CATEGORY_NAME, ((AircountrApplication) getActivity().getApplicationContext()).mCategoryListItems.get(position).getCategoryName());
                startActivity(intent);
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
