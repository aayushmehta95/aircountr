package com.android.aircountr;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.aircountr.constants.CreateUrl;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by gaura on 6/8/2016.
 */
public class ForgotPasswordActivity extends BaseActivity {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private RadioGroup rg_option;
    private RadioButton rb_phone;
    private RadioButton rb_email;
    private LinearLayout ll_emailLayout;
    private EditText et_emailId;
    private TextView tv_errEmailId;
    private LinearLayout ll_phoneLayout;
    private EditText et_phoneNumber;
    private TextView tv_errPhoneNumber;
    private TextView tv_getPasswordBtn;
    private String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordActivity.this.finish();
            }
        });

        rg_option.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_email) {
                    rb_email.setTextColor(getResources().getColor(R.color.black));
                    rb_phone.setTextColor(getResources().getColor(R.color.light_grey));
                    ll_emailLayout.setVisibility(View.VISIBLE);
                    ll_phoneLayout.setVisibility(View.GONE);
                } else {
                    rb_email.setTextColor(getResources().getColor(R.color.light_grey));
                    rb_phone.setTextColor(getResources().getColor(R.color.black));
                    ll_emailLayout.setVisibility(View.GONE);
                    ll_phoneLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        tv_getPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checked = rg_option.getCheckedRadioButtonId();
                if (checked == R.id.rb_email) {
                    checkEmail(et_emailId.getText().toString().trim());
                } else if (checked == R.id.rb_phone) {
                    checkPhone(et_phoneNumber.getText().toString().trim());
                }
            }
        });

        et_emailId.addTextChangedListener(new GenericTextWatcher(et_emailId));
        et_phoneNumber.addTextChangedListener(new GenericTextWatcher(et_phoneNumber));
    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        rg_option = (RadioGroup) findViewById(R.id.rg_option);
        rb_phone = (RadioButton) findViewById(R.id.rb_phone);
        rb_email = (RadioButton) findViewById(R.id.rb_email);
        ll_emailLayout = (LinearLayout) findViewById(R.id.ll_emailLayout);
        et_emailId = (EditText) findViewById(R.id.et_emailId);
        tv_errEmailId = (TextView) findViewById(R.id.tv_errEmailId);
        ll_phoneLayout = (LinearLayout) findViewById(R.id.ll_phoneLayout);
        et_phoneNumber = (EditText) findViewById(R.id.et_phoneNumber);
        tv_errPhoneNumber = (TextView) findViewById(R.id.tv_errPhoneNumber);
        tv_getPasswordBtn = (TextView) findViewById(R.id.tv_getPasswordBtn);

        tv_pageTitle.setTypeface(SEMIBOLD);
        rb_email.setTypeface(REGULAR);
        rb_phone.setTypeface(REGULAR);
        et_emailId.setTypeface(REGULAR);
        tv_errEmailId.setTypeface(REGULAR);
        et_phoneNumber.setTypeface(REGULAR);
        tv_errPhoneNumber.setTypeface(REGULAR);
        tv_getPasswordBtn.setTypeface(REGULAR);
    }


    private void checkEmail(String emailId) {
        if (emailId.isEmpty()) {
            tv_errEmailId.setVisibility(View.VISIBLE);
            tv_errEmailId.setText("Plz enter email address");
        } else if (!emailId.matches(EMAIL_PATTERN)) {
            tv_errEmailId.setVisibility(View.VISIBLE);
            tv_errEmailId.setText("Email address not valid");
        } else {
            tv_errEmailId.setVisibility(View.GONE);
            sendForgotPassRequest("email", emailId);
        }
    }

    private void checkPhone(String phoneNumber) {
        if (phoneNumber.isEmpty()) {
            tv_errPhoneNumber.setVisibility(View.VISIBLE);
            tv_errPhoneNumber.setText("Plz enter phone number");
        } else if (phoneNumber.length() < 10) {
            tv_errPhoneNumber.setVisibility(View.VISIBLE);
            tv_errPhoneNumber.setText("enter 10 digits phone number");
        } else {
            tv_errPhoneNumber.setVisibility(View.GONE);
            sendForgotPassRequest("mobile", phoneNumber);
        }
    }

    private void sendForgotPassRequest(String key, String value) {
        if (isNetworkAvailable()) {
            new ForgotPasswordAsync().execute(key, value);
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }
    }

    private class ForgotPasswordAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "Response : " + s);
            if (s != null) {
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        displayToast(msg);
                        ForgotPasswordActivity.this.finish();
                    } else {
                        showDialog("Alert", msg, "OK");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.forgotPasswordUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(params[0], params[1]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.et_emailId:
                    if (text.length() > 0) {
                        if (text.matches(EMAIL_PATTERN))
                            tv_errEmailId.setVisibility(View.GONE);
                        else {
                            tv_errEmailId.setVisibility(View.VISIBLE);
                            tv_errEmailId.setText("Plz enter valid email address");
                        }
                    } else {
                        tv_errEmailId.setVisibility(View.VISIBLE);
                        tv_errEmailId.setText("Plz enter email address");
                    }
                    break;
                case R.id.et_phoneNumber:
                    if (text.length() > 0)
                        if (text.length() != 10) {
                            tv_errPhoneNumber.setVisibility(View.VISIBLE);
                            tv_errPhoneNumber.setText("Plz enter 10 digits valid phone number");
                        } else
                            tv_errPhoneNumber.setVisibility(View.GONE);
                    else {
                        tv_errPhoneNumber.setVisibility(View.VISIBLE);
                        tv_errPhoneNumber.setText("Plz enter phone number");
                    }
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }
}
