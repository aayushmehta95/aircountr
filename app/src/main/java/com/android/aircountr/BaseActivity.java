package com.android.aircountr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by gaurav on 4/16/2016.
 */
public class BaseActivity extends Activity {

    public Typeface REGULAR, SEMIBOLD;
    //    public ProgressDialog processing;
    public MaterialDialog processing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");
    }

    public <T> void switchActivity(Context context, Class<T> startActivity) {

        Intent intent = new Intent(context, startActivity);
        startActivity(intent);
    }

    public void displayToast(int id) {
        Toast.makeText(getApplicationContext(), getResources().getString(id),
                Toast.LENGTH_SHORT).show();
    }

    public void displayToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public void showDialog(String title, String msg, String btnText) {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(msg)
                .backgroundColorRes(R.color.white)
                .titleColorRes(R.color.black)
                .contentColor(Color.BLACK)
                .positiveColorRes(R.color.theme_color_blue)
                .buttonRippleColorRes(R.color.transparent_theme_color)
                .positiveText(btnText)
                .cancelable(false)
                .show();
    }

    public void showLoading(String message, boolean cancelable) {
        processing = new MaterialDialog.Builder(this)
                //.title(R.string.progress_dialog)
                .content(message)
                .progress(true, 0)
                .backgroundColorRes(R.color.white)
                .contentColor(getResources().getColor(R.color.black))
                .cancelable(cancelable)
                .show();
    }

    public void hideLoading() {
        if (processing != null)
            processing.dismiss();
    }

}
